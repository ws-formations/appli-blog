### Git global setup ###

```
git config --global user.name "SOW Mamadou Oury"
git config --global user.email "mamadououry.sow@scalian.com"
```

### Git global setup ###

```
ng new form-ang-bookshelf --style=scss --skip-tests=true
npm install bootstrap@3.3.7 --save
```

```
ng g c auth/signup
ng g c auth/signin
ng g c book-list
ng g c book-list/single-book
ng g c book-list/book-form
ng g c header
ng g s services/auth
ng g s services/books
ng g s services/auth-guard
```

### Ajout Bootstrap ###

```
npm install bootstrap@3.3.7 --save
npm install bootstrap@4.3.1 --save
```
Dans  **options** -> angular.json
```
"styles": [
        "./node_modules/bootstrap/dist/css/bootstrap.css",
        "styles.css"
  ],
```
### Intégrez Firebase à votre application ###

```
npm install firebase --save
```

```
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.19.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyBV_PPpVwq9SjPwpkjsJ9Anb0cSmnysG3Q",
    authDomain: "bookshelves-394ea.firebaseapp.com",
    databaseURL: "https://bookshelves-394ea.firebaseio.com",
    projectId: "bookshelves-394ea",
    storageBucket: "bookshelves-394ea.appspot.com",
    messagingSenderId: "193559765744",
    appId: "1:193559765744:web:170e2b3c01150124d41835"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>

```
### Internationalization ngx-translate ###
```
npm install @ngx-translate/core @ngx-translate/http-loader

```
### Site des dependances ###
https://www.npmjs.com/

font-awesome
```
npm i font-awesome
```
