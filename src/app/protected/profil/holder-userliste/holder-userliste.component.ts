import {Component, OnDestroy, OnInit} from '@angular/core';
import {HolderuserService} from '../../../core/services/holderuser.service';
import {RestError} from '../../../shared/model/RestError.model';
import {HolderUser} from '../../../shared/model/HolderUser.model';
import {Observable, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-holder-userliste',
  templateUrl: './holder-userliste.component.html',
  styleUrls: ['./holder-userliste.component.scss']
})
export class HolderUserlisteComponent implements OnInit, OnDestroy {

  restError: RestError;

  mesimages = '/assets/mesimages/ile.jpg';

  users: HolderUser[] = [];
  holderUser: HolderUser;
  userSubscription: Subscription;
  holderUser$: Observable<HolderUser>;


  constructor(private holderuserService: HolderuserService) {
  }

  ngOnInit(): void {
    // this.listUsersAPIObservable();
    this.getListUsersBySubject();
    // this.getUserBySubject();
  }

  getListUsersBySubject(): void {
    // On emet le subject
    this.holderuserService.listUsersAPIPromiseBySbject();
    // On  souscrit au Subject (userSubject) dans  UserService pour emmetre l'objet user
    this.userSubscription = this.holderuserService.holderListeUserSubject
      .subscribe(
        (users: HolderUser[]) => {
          // users recu depuis le subject (userSubject)
          this.users = users;
          console.log('4 END SUBSCRIB', this.users);
        }
      );
  }

  getUserBySubject(): void {
    // On emet le subject
    this.holderuserService.getUserAPIPromiseBySbject();
    // On  souscrit au Subject (userSubject) dans  UserService pour emmetre l'objet user
    this.userSubscription = this.holderuserService.holderUserSubject
      .subscribe(
        (user: HolderUser) => {
          // users recu depuis le subject (userSubject)
          this.holderUser = user;
          this.users[user.id - 1] = user;
          console.log('4 END SUBSCRIB ' + user.id, this.holderUser);
        }, (eror) => {
          console.log(' Hoo laa Component off ' + eror);
        }
      );
  }

  listUsersAPIPromiseBySbject(): void {
    this.holderuserService.listUsersAPIPromiseBySbject()
      .then(
        (data) => {
          console.log(data.valueOf());
          this.users = data ? data : [];
        })
      .catch((error) => {
        console.log(' Hoo laa Component off ' + error);
      });

  }

  listUsersAPIObservable(): void {
    this.holderuserService.listUsersAPIObservable()
      .pipe(
        map(holderUsers => holderUsers.filter(usr => usr.id <= 6)
        ))
      .subscribe(
        (data) => {
          console.log(data.valueOf());
          this.users = data ? data : [];
        }, (error) => {
          this.restError = error.error;
          console.log(this.restError);
        }
      );
  }
  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
