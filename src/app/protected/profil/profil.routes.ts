import {Route} from '@angular/router';
import {NewUserComponent} from './new-user/new-user.component';
import {UserListComponent} from './user-list/user-list.component';
import {HolderUserlisteComponent} from './holder-userliste/holder-userliste.component';
import {AutoCompleteComponent} from './auto-complete/auto-complete.component';


export const profilRoutes: Route[] = [
  { path: 'new-user', component: NewUserComponent },
  { path: 'users', component: UserListComponent },
  { path: 'holderUsers', component: HolderUserlisteComponent },
  { path: 'country', component: AutoCompleteComponent }
];
