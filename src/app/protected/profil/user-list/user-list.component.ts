import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../shared/model/User.model';
import {Subscription} from 'rxjs';
import {UserService} from '../../../core/services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  users: User[];
  userSubscription: Subscription;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    //  souscrit au Subject (userSubject) dans  UserService pour emmetre l'objet user
    // (users: User[]) => {this.users = users;}
    this.userSubscription = this.userService.userSubject.subscribe(
      (users: User[]) => {
        // users recu depuis le subject (userSubject)
        this.users = users;
      }
    );
    // On emet le subject
    this.userService.listUsers();
    this.userService.emitListUsers();
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  onDeleteUser(user: User) {
    this.userService.removeUser(user);
  }

  onNewUser() {
   this.router.navigate(['/profil', 'new-user']);
  }
}

