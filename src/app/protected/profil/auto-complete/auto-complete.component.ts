import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, switchMap} from 'rxjs/operators';
import {AutoCompleteService} from '../../../core/services/AutoComplete.service';
import {Country} from '../../../shared/model/restcountry/Country';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.scss']
})
export class AutoCompleteComponent implements OnInit {
  countryControl: FormControl = new FormControl();
  countryListe$: Observable<Country[]>;

  countries: string[] = [];
  pays: Country;
  country: Country[] = [];
  name = '';
  public placeholder = 'Enter le nom de la ville';
  public keyword = 'name';
  public keywords = ['name', 'capital', 'region'];
  public historyHeading = 'Recently selected';

  page = 1;
  pageSize = 25;
  collectionSize = 0;

  constructor(private autoCompleteService: AutoCompleteService) {
    this.refreshCountries();
  }

  ngOnInit(): void {
    this.countryListe$ = this.autoCompleteService.listCitybservable();
   // this.listCitybservable();
  }

  listCitybservable(): void {
    this.autoCompleteService.listCitybservable()
      .subscribe(
        (contries) => {
          this.country = contries;
          console.log('**** this.country ***', this.country);
        });
  }

  selectEvent(item) {
    // do something with selected item
    this.pays = item;
    console.log('**** selectEvent :  Objet selectionné ***', item);
  }

  onFocused(focused) {
    // do something when input is focused
    console.log('**** focused ***', focused);
  }

  /**
   * Submit template form
   */
  submitTemplateForm(value) {
    console.log('Submit template form ' + value);
  }

  onChangeSearch(val: string) {
    console.log('*** onChangeSearch ***', val);
    this.countryControl.valueChanges
      .pipe(
        map(name => name.trim()),
        filter(name => length >= 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(name => this.autoCompleteService.search(name))
      )
      .subscribe(
        (city => {
          this.countries = city;
          console.log('**** this.countries ***', this.countries);
        })
      );
  }



  search(): void {
    const nom = 'Leanne';
    console.log(nom);

    this.autoCompleteService.listCitybservable()
      .pipe(
        map(usrs => usrs.filter(usr => nom.trim())),
        filter(name => length >= 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(usrs => usrs.filter(usr => this.autoCompleteService.search(nom)))
      ).subscribe(resul => {
      console.log('*************************', resul);
    });
  }

  refreshCountries(): void {
    this.autoCompleteService.listCitybservable()
      .subscribe(
      data => {
        this.country = data;
        this.collectionSize = this.country.length;
        this.country.map((country, i) => ({id: i + 1, ...country}))
          .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
        console.log('**** this.countries ***', this.countries);
      });
  }
}
