import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {UserListComponent} from './user-list/user-list.component';
import {NewUserComponent} from './new-user/new-user.component';
import {profilRoutes} from './profil.routes';
import {HolderUserlisteComponent} from './holder-userliste/holder-userliste.component';
import {AutoCompleteComponent} from './auto-complete/auto-complete.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [UserListComponent, NewUserComponent, HolderUserlisteComponent, AutoCompleteComponent],
  exports: [UserListComponent, NewUserComponent],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forChild(profilRoutes),
  ]
})
export class ProfilModule {
}
