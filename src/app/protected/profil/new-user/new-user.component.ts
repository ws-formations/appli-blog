import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../../core/services/user.service';
import {User} from '../../../shared/model/User.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  userFormGroup: FormGroup;
  private userSubscription: Subscription;
  private users: User[] = [];

  fileIsUploading = false;
  fileUrl: string;
  fileUploaded = false;
  /**
   * FormGroup: ReactiveForme Module fonctionne avec FormGroup
   * FormBuilder: outil qui permet des créer des formulaire plus facilement
   */
  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  /**
   * On initialise le formulaire
   * La méthode  group  prend comme argument un objet où les clés correspondent aux noms des contrôles souhaités
   * et les valeurs correspondent aux valeurs par défaut de ces champs.
   * Puisque l'objectif est d'avoir des champs vides au départ, chaque valeur ici correspond au string vide.
   * Validators: Est un outil pour la validation de données dans la méthode réactive
   * ['', Validators.required]: '' signifie que la valeur par defaut des champs est un string vide
   */
  initForm(): void {
    this.userFormGroup = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      drinkPreference: ['preference', Validators.required],
      hobbies: this.formBuilder.array([])  // Ajout dynamique des FormControl ici c'est des hobbies qu'on ajoute
    });
  }

  /**
   * formValue.hobbies ? formValue.hobbies : []
   * Cette ligne verifie s'il existe des hobbies si non ca sera un tableau vide
   */
  onSubmitForm(): void {
    const formValueUser = this.userFormGroup.value;

    const firstName = formValueUser.firstName;
    const lastName  = formValueUser.lastName;
    const email     = formValueUser.email;
    const drinkPref = formValueUser.drinkPreference;
    // Ajout dynamique des FormControl ici c'est des hobbies qu'on ajoute
    const hobbies   = formValueUser.hobbies ? formValueUser.hobbies : [];

    const newUser = new User(firstName, lastName, email, drinkPref, hobbies);

    if (this.fileUrl && this.fileUrl !== '') {
      newUser.photo = this.fileUrl;
    }
    this.userService.createNewUser(newUser);
    this.router.navigate(['/profil', 'users']);
  }

  /**
   * afin d'avoir accès aux  controls  à l'intérieur de l'array,
   * pour des raisons de typage strict liées à TypeScript,
   * il faut créer une méthode qui retourne  hobbies  par la méthode  get()  sous forme de  FormArray
   */
  getHobbies(): FormArray {
    return this.userFormGroup.get('hobbies') as FormArray;
  }

  /**
   * créer la méthode qui permet d'ajouter un  FormControl  à  hobbies,
   * permettant ainsi à l'utilisateur d'en ajouter autant qu'il veut.
   * Vous allez également rendre le nouveau champ requis, afin de ne pas avoir un array de  hobbies  avec des string vides:
   */
  onAddHobby(): void {
    const newHobbyControl = this.formBuilder.control(null, Validators.required);
    this.getHobbies().push(newHobbyControl);
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.userService.uploadFile(file).then(
      (url: string) => {
        this.fileUrl = url;
        console.log('fileUrl = ' + this.fileUrl);
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    );
  }

  detectFiles(event) {
    this.onUploadFile(event.target.files[0]);
  }

}
