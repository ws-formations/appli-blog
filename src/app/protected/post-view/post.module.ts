import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {PostViewComponent} from './post-view.component';
import {postRoutes} from './post.routes';
import {SinglePostComponent} from './single-post/single-post.component';
import {PostItemComponent} from './post-item/post-item.component';
import {NewPostComponent} from './new-post/new-post.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [PostViewComponent, SinglePostComponent, PostItemComponent, NewPostComponent],
  exports: [PostViewComponent, SinglePostComponent, PostItemComponent, NewPostComponent],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forChild(postRoutes),
  ]
})
export class PostModule {
}
