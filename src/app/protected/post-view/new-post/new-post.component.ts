import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../../../shared/model/post.model';
import {PostService} from '../../../core/services/post.service';
import {Router} from '@angular/router';
import {PostFirebaseService} from '../../../core/services/post-firebase.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  private postSubscription: Subscription;
  private posts: Post[] = [];
  constructor(private postService: PostService, private postFirebaseService: PostFirebaseService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(ngForm: NgForm): void {
    console.log(ngForm.value);
    const title = ngForm.value.title;
    const content = ngForm.value.content;
    const post = new Post(title, content, 0, ngForm.value.dateCreation);
    this.postFirebaseService.createNewPost(post);
    this.router.navigate(['/posts']);
  }

  postListFromLocal(): void {
    // On sourcrit au subject des appareils de type any
    this.postSubscription = this.postService.postSubject.subscribe(
      (newPosts: Post[]) => {
         this.posts = newPosts;
      }
    );
    // En fin on emet la sourcription du subject
    this.postService.emitListPosts();
  }

  onSubmitToLocal(ngForm: NgForm): void {
    console.log(ngForm.value);
    const title = ngForm.value.title;
    const content = ngForm.value.content;
    const post = new Post(title, content, 0, new Date());
    this.postService.addNewPost(post);

    this.router.navigate(['/posts']);
  }
}
