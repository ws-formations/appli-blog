import {Component, OnInit} from '@angular/core';
import {PostService} from '../../../core/services/post.service';
import {ActivatedRoute} from '@angular/router';
import {Post} from '../../../shared/model/post.model';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit {
   postTitle: string;
   postContent: string;
   postLove: number;
   createdDate: Date;
   post: Post;
  constructor(private postService: PostService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getPostById();
  }

  getPostById(): void{
    const idPost = this.route.snapshot.params.idPost;
    if (idPost){
      this.post = this.postService.getPostById(+idPost);
      console.log(idPost);
    }
    if (this.post) {
      this.postTitle = this.post.title;
      this.postContent = this.post.content;
      this.postLove = this.post.loveIts;
      this.createdDate = this.post.createdat;
    }
  }

  getColor(): string {
   return this.postService.getColor(this.postLove);
  }

}
