import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../../../shared/model/post.model';
import {ActivatedRoute, Router} from '@angular/router';
import {PostService} from '../../../core/services/post.service';
import {PostFirebaseService} from '../../../core/services/post-firebase.service';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss']
})
export class PostItemComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLove: number;
  @Input() createdDate: Date;
  @Input() idPost: number;
  @Input() indexOfPost: number;
  @Input() postInput: Post;

  like = 'Like';

  constructor(private route: ActivatedRoute,
              private router: Router, private postService: PostService, private postFirebaseService: PostFirebaseService) {
  }

  ngOnInit(): void {
  }

  onViewPost(id: number): void {
    console.log('id = ' + id + ' ' + this.idPost + ' postInput = ', this.postInput);
    this.router.navigate(['/posts', +id]);
  }

  onLoveIt(): void {
    this.postFirebaseService.onLovePost(this.indexOfPost);
  }

  onDontLoveIt(): void {
    this.postFirebaseService.onDontLovePost(this.indexOfPost);
  }

  onDeletePost(post: Post): void {
    this.postFirebaseService.removeBook(post);
  }

  onDeletPostLocal(): void {
    console.log(this.indexOfPost);
    this.postService.removePost(this.indexOfPost);
  }

  getColor(): string {
    this.like = this.postLove >= 0 ? 'Like' : 'Dislike';
    return this.postFirebaseService.getColor(this.postLove);
  }
}
