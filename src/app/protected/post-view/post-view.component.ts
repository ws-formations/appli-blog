import {Component, OnInit} from '@angular/core';
import {Post} from '../../shared/model/post.model';
import {PostService} from '../../core/services/post.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {PostFirebaseService} from '../../core/services/post-firebase.service';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {

  posts: Post[];
  postSubscription: Subscription;

  // On utilise l'instance PostService injecté dans appModule
  constructor(private postService: PostService, private postFirebaseService: PostFirebaseService, private router: Router) {
  }

  // cette fonction sera executé au moment de la création de AppComponent par angular et c'est l'exécution du constructeur
  ngOnInit(): void {
    this.onFetch();
    // On sourcrit au subject des appareils de type any
    this.postSubscription = this.postFirebaseService.postSubject.subscribe(
      (newPosts: Post[]) => {
        this.posts = newPosts;
      }
    );
    // En fin on emet la sourcription du subject
    this.postFirebaseService.emitListPosts();

  }

  onNewPost(): void {
    this.router.navigate(['/posts', 'newpost']);
  }

  onFetch(): void {
    this.postFirebaseService.getPosts();
  }
}
