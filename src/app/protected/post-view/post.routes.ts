import {Route} from '@angular/router';
import {PostViewComponent} from './post-view.component';
import {NewPostComponent} from './new-post/new-post.component';
import {SinglePostComponent} from './single-post/single-post.component';


export const postRoutes: Route[] = [
  { path: '', component: PostViewComponent},
  { path: 'newpost', component: NewPostComponent },
  { path: ':idPost', component: SinglePostComponent }
];
