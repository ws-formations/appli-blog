import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ProtectedComponent} from './protected.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  imports: [RouterModule, HttpClientModule, FormsModule, SharedModule],
  declarations: [ProtectedComponent],
  exports: [ProtectedComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class ProtectedModule { }
