import {Route} from '@angular/router';
import {ProtectedComponent} from './protected.component';
import {AuthGuardFirebase} from '../core/services/auth-guard-firebase.service';

export const ProtectedRoutes: Route[] = [
  {
    path: '', component: ProtectedComponent,
    canActivate: [AuthGuardFirebase],
    children: [
      {path: 'profil', loadChildren: () => import('.//profil/profil.module').then(m => m.ProfilModule)},
      {path: 'posts', loadChildren: () => import('.//post-view/post.module').then(m => m.PostModule)}
    ]
  }
];
