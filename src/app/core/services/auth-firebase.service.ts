import {Injectable} from '@angular/core';
import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class AuthFirebaseService {

  constructor() { }

  createNewUser(email: string, password: string): Promise<any>{
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  signInUser(email: string, password: string): Promise<any> {
    const promise =  new Promise(
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
    this.isUserAuthentifie();
    return promise;
  }

  isUserAuthentifie(): boolean {
    let isAuth = false;
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          isAuth = true;
        } else {
          isAuth = false;
        }
      }
    );
    return isAuth;
  }

  signOutUser(): void {
    firebase.auth().signOut();
  }
}
