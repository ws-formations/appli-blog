import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {RestError} from '../../shared/model/RestError.model';
import {HolderUser} from '../../shared/model/HolderUser.model';

@Injectable({
  providedIn: 'root'
})
export class HolderuserService {

  url = 'https://my-json-server.typicode.com/mourysow/jsonserver/users/';
  // un subject qui emettra des un tableau de type USER
  holderListeUserSubject = new Subject<HolderUser[]>();
  holderUserSubject = new Subject<HolderUser>();
  restError: RestError;

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Une methode qui va emettre une copie du tableau USER
   */
  emitListHolderUsers(users: HolderUser[]): void {
    console.log('3 - START EMIT: ', users[0]);
    this.holderListeUserSubject.next(users.slice());
    console.log('END EMIT - >', users[0]);
  }

  emitListHolderUser(user: HolderUser): void {
    console.log('3 - START EMIT: ', user);
    this.holderUserSubject.next(user);
    console.log('END EMIT - >', user);
  }

  listUsersAPIPromiseBySbject(): Promise<HolderUser[]> {
    console.log('1- START HTTP GET RESULT -> ');
    const promiseUser = new Promise<HolderUser[]>(
      (resolve, reject) => {
        return this.httpClient.get<HolderUser[]>(this.url, {responseType: 'json'}).toPromise()
          .then(
            (data) => {
              console.log('2- RESULT HTTP GET RESULT -> ', data.valueOf());
              const users = data ? data : [];
              this.emitListHolderUsers(users);
              resolve(data);
            }, (error) => {
              console.log(' Hoo laa service off ' + error);
              reject(error);
            }
          );
      }
    );
    return promiseUser;
  }
  sleep(ms) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve();
      }, ms * 1000);
    });
  }
   getUserAPIPromiseBySbject(): Promise<HolderUser> {
    console.log('1- START HTTP GET RESULT -> ');
    const promiseUser = new Promise<HolderUser>(
      (resolve, reject) => {
        this.httpClient.get<HolderUser[]>(this.url, {responseType: 'json'}).toPromise()
          .then(
            (data) => {
              console.log('2- RESULT HTTP GET RESULT -> ', data.valueOf());
              const users = data ? data : [];
              for (const usr of users){
                if (usr){
                  this.emitListHolderUser(usr);
                  resolve(usr);
                }
              }

            }, (error) => {
              console.log(' Hoo laa service off ' + error);
              reject(error);
            }
          );
      }
    );
    return promiseUser;
  }

  listUsersAPIObservable(): Observable<HolderUser[]> {
    return this.httpClient.get<HolderUser[]>(this.url, {responseType: 'json'});
  }

  getUserParID(idUser: number): Promise<HolderUser> {
    return new Promise(
      (resolve, reject) => {
        return this.httpClient.get<HolderUser>(this.url + idUser, {responseType: 'json'}).toPromise().then(
          (data) => {
            console.log(data);
            resolve(data);
          }, (error) => {
            console.log(this.restError);
            reject(error);
          }
        );
      }
    );
  }

}
