import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {Country} from '../../shared/model/restcountry/Country';

@Injectable({
  providedIn: 'root'
})
export class AutoCompleteService {

  constructor(private httpClient: HttpClient) {
  }

  listCitybservable(): Observable<Country[]> {
    const   urls = 'https://restcountries.eu/rest/v2/all';
    return this.httpClient.get<Country[]>(urls, {responseType: 'json'});
  }

  search(name: string): Observable<string[]> {
    // Afghanistan
    console.log('///////// name //////// ' + name);
    name = name && name.trim();
    if (name) {
      const   url = 'https://restcountries.eu/rest/v2/name/' + name;
      return this.httpClient.get<Country[]>(url + name)
        .pipe(
            map(countries =>
              countries.map(contry => contry.translations.fr)
            ),
          catchError(error => of([]))
      );
    }
    return of([]);
  }
}
