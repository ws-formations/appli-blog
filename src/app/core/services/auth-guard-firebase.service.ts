import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import * as firebase from 'firebase';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardFirebase {

  constructor(private router: Router) { }

  /**
   * Ici la fonction canActivate va retourner une Promise
   * onAuthStateChanged: va retourner un user si ce user existe dans firebase
   * auth/signin dans navigate s'ecrit: ['/auth', 'signin']
   */
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().onAuthStateChanged(
          (user) => {
            if (user) {
              resolve(true);
            } else {
              this.router.navigate(['/auth', 'signin']);
              resolve(false);
            }
          }
        );
      }
    );
  }
}
