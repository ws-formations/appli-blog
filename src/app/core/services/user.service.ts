import {User} from '../../shared/model/User.model';
import {Observable, Subject} from 'rxjs';
import * as firebase from 'firebase';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {RestError} from '../../shared/model/RestError.model';
import {APIURL} from '../../shared/model/api-url';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = APIURL.baseUrl + '/app';
  // un subject qui emettra des un tableau de type USER
  userSubject = new Subject<User[]>();
  restError: RestError;
  private users: User[] = [];

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Une methode qui va emettre une copie du tableau USER
   */
  emitListUsers(): void {
    this.userSubject.next(this.users.slice());
  }

  saveUsers(): void {
    console.log('saveUsers to firebase: ', this.users);
    firebase.database().ref('/users').set(this.users);
  }

  /**
   * Ajouter un USER et emettre le USER
   */
  createNewUser(newUser: User): void {
    let id = 1;
    if (this.users.length > 1) {
      id = this.users[(this.users.length - 1)].id + 1;
    }
    newUser.id = id;
    console.log('createNewUser with id = : ', newUser);
    this.users.push(newUser);
    this.saveUsers();
    this.emitListUsers();
  }

  getSingleUserByID(id: number): Promise<User> {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/users/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
  }

  listUsers(): void {
    //  this.listUsersFirebase();
    this.listUsersAPIObservable();
    // this.listUsersAPIPromise();
  }


  listUsersFirebase(): void {
    firebase.database().ref('/users')
      .on('value', (data) => {
          console.log(data.val());
          this.users = data.val() ? data.val() : [];
          this.emitListUsers();
        }
      );
  }

  listUsersAPIPromise(): Promise<User[]> {

    const promiseUser = new Promise<User[]>(
      (resolve, reject) => {
        const apiURL = this.url + '/user/users';
        return this.httpClient.get<User[]>(apiURL, {responseType: 'json'}).toPromise()
          .then(
            (data) => {
              console.log(data.valueOf());
              this.users = data ? data : [];
              this.emitListUsers();
              resolve(data);
            }, (error) => {
              this.restError = error;
              console.log(this.restError);
              reject(error);
            }
          );
      }
    );
    return promiseUser;
  }

  listUsersAPIObservable(): Observable<User[]> {
    const apiURL = this.url + '/user/users';
    const observableUser = this.httpClient.get<User[]>(apiURL, {responseType: 'json'});

    observableUser.subscribe(
      (data) => {
        console.log(data.valueOf());
        this.users = data ? data : [];
        this.emitListUsers();
      }, (err) => {
      console.log('Erreur pendant la recherche de la liste des user ', err);
    });

    return observableUser;
  }

  countUsers(): Promise<number> {
    return new Promise(
      (resolve, reject) => {
        const apiURL = this.url + '/user/cont';
        return this.httpClient.get<number>(apiURL, {responseType: 'json'}).toPromise().then(
          (data) => {
            console.log(data.valueOf());
            return resolve(data.valueOf());
          }, (error) => {
            this.restError = error.error;
            console.log(this.restError);
            reject(error);
          }
        );
      }
    );
  }

  getUserParID(idUser: number): Promise<number> {
    return new Promise(
      (resolve, reject) => {
        const apiURL = this.url + '/user/' + idUser;
        return this.httpClient.get<number>(apiURL, {responseType: 'json'}).toPromise().then(
          (data) => {
            console.log(data.valueOf());
            return resolve(data.valueOf());
          }, (error) => {
            this.restError = error.error;
            console.log(this.restError);
            reject(error);
          }
        );
      }
    );
  }


  userById(idUser: number): Observable<User> {
    return this.httpClient.get<User>(this.url + '/user/' + idUser, {responseType: 'json'});
  }


  removeUser(user: User): void {
    const userIndexToRemove = this.users.findIndex(
      (userEl) => {
        if (userEl === user) {
          return true;
        }
      }
    );
    this.users.splice(userIndexToRemove, 1);
    this.saveUsers();
    this.emitListUsers();
  }

  uploadFile(file: File) {
    return new Promise(
      (resolve, reject) => {
        const almostUniqueFileName = Date.now().toString();
        console.log('images/' + almostUniqueFileName + file.name);
        const upload = firebase.storage().ref().child('images/' + almostUniqueFileName + file.name).put(file);

        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          () => {
            console.log('Chargement en cours…');
          },
          (error) => {
            console.log('Erreur de chargement ! : ' + error.message + error);
            reject();
          },
          () => {
            resolve(upload.snapshot.ref.getDownloadURL());
          }
        );
      }
    );
  }


}
