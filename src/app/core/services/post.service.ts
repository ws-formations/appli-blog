import {Injectable} from '@angular/core';
import {Post} from '../../shared/model/post.model';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private posts: Post[] = [
    { id: 1, title: 'Mon premier post', content: 'Lorem Ipsum is simply Mon premier post.', loveIts: 1, createdat: new Date()},
    {id: 2, title: 'Mon deuxème post', content: 'Lorem Ipsum is simply Mon deuxème post', loveIts: 0, createdat: new Date()},
    {id: 3, title: 'Mon troisième post', content: 'Lorem Ipsum is Mon troisième post', loveIts: -1, createdat: new Date()},
    { id: 4, title: 'Encore un post', content: 'Lorem Ipsum is simply Encore un post', loveIts: 1, createdat: new Date()}
  ];

  postSubject = new Subject<Post[]>();

  /**
   * Une methode qui va emettre une copie du tableau des posts
   * Le Subject (postsSubject) va emettre la liste des posts
   * Next: push -> la methode next va forcer le Subject à emettre ce qu'on lui passe comme argument
   * slice() va emmettre une copy du tableau posts
   */
  emitListPosts(): void {
    this.postSubject.next(this.posts.slice());
  }

  onLovePost(index: number): void {
    this.posts[index].loveIts =  this.posts[index].loveIts + 1;
    this.emitListPosts();
  }

  onDontLovePost(index: number): void {
    this.posts[index].loveIts =  this.posts[index].loveIts - 1;
    this.emitListPosts();
  }

  /**
   * find: pour cherhcher dans le tableau
   */
  getPostById(id: number): Post {
    const post = this.posts.find(
      (monPost) => {
        return monPost.id === id;
      }
    );
    return post;
  }

  /**
   * Ajouter un Post et emettre le Post
   */
  addNewPost(post: Post): void {
    post.id = this.posts[(this.posts.length - 1)].id + 1;
    this.posts.push(post);
    this.emitListPosts();
  }

  removePost(index: number): void {
    this.posts.splice(index, 1);
    this.emitListPosts();
  }

  getColor(postLove: number): string {
    if (postLove > 0) {
      return 'green';
    } else if (postLove < 0) {
      return 'red';
    } else if (postLove === 0) {
      return 'black';
    }
  }

}
