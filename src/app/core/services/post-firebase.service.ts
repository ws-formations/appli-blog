import {Injectable} from '@angular/core';
import {Post} from '../../shared/model/post.model';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostFirebaseService {
  private posts = [];

  postSubject = new Subject<Post[]>();

  constructor(private httpClient: HttpClient) {
  }
  /**
   * Une methode qui va emettre une copie du tableau des posts
   * Le Subject (postsSubject) va emettre la liste des posts
   * Next: push -> la methode next va forcer le Subject à emettre ce qu'on lui passe comme argument
   * slice() va emmettre une copy du tableau posts
   */
  emitListPosts(): void {
    this.postSubject.next(this.posts.slice());
  }

  onLovePost(index: number): void {
    this.posts[index].loveIts =  this.posts[index].loveIts + 1;
    this.savePosts();
    this.emitListPosts();
  }

  onDontLovePost(index: number): void {
    this.posts[index].loveIts =  this.posts[index].loveIts - 1;
    this.savePosts();
    this.emitListPosts();
  }

  savePosts(): void {
    console.log('savePosts to firebase: ', this.posts);
    firebase.database().ref('/posts').set(this.posts);
  }

  createNewPost(newPost: Post): void {
    let id = 1;
    if (this.posts.length > 1) {
      id = this.posts[(this.posts.length - 1)].id + 1;
    }
    newPost.id = id;
    console.log('createNewPost with id = : ', newPost);
    this.posts.push(newPost);
    this.savePosts();
    this.emitListPosts();
  }

  getSinglePost(id: number): Promise<Post>{
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/posts/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
  }

  getPosts(): void {
    firebase.database().ref('/posts')
      .on('value', (data) => {
          this.posts = data.val() ? data.val() : [];
          this.emitListPosts();
        }
      );
  }

  removeBook(post: Post): void {
    const postIndexToRemove = this.posts.findIndex(
      (postEl) => {
        if (postEl === post) {
          return true;
        }
      }
    );
    this.posts.splice(postIndexToRemove, 1);
    this.savePosts();
    this.emitListPosts();
  }

  savePostsToServer(): void {
    this.httpClient
      .put('https://tutosangular.firebaseio.com/posts.json', this.posts)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
  getPostsFromServer(): void {
    this.httpClient
      .get<any[]>('https://tutosangular.firebaseio.com/posts.json')
      .subscribe(
        (response) => {
          this.posts = response;
          console.log('response ! : ' + this.posts);
          this.emitListPosts();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
    this.emitListPosts();
  }

  getColor(postLove: number): string {
    if (postLove > 0) {
      return 'green';
    } else if (postLove < 0) {
      return 'red';
    } else if (postLove === 0) {
      return 'black';
    }
  }
}
