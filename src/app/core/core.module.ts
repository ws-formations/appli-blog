import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PublicModule} from '../public/public.module';
import {PostService} from './services/post.service';
import {AuthService} from './services/auth.service';
import {AuthFirebaseService} from './services/auth-firebase.service';
import {AuthGuard} from './services/auth-guard.service';
import {AuthGuardFirebase} from './services/auth-guard-firebase.service';
import {UserService} from './services/user.service';
import {NotFoudModule} from '../not-foud/not-found.module';
import {ProtectedModule} from '../protected/protected.module';
import {HolderuserService} from './services/holderuser.service';

@NgModule({
  declarations: [],
  imports: [
    PublicModule,
    ProtectedModule,
    NotFoudModule,
    CommonModule
  ],
  providers: [
    PostService,
    AuthService,
    AuthFirebaseService,
    AuthGuard,
    AuthGuardFirebase,
    UserService,
    HolderuserService],
  exports: []
})
export class CoreModule {
  // CoreModule ne doit être importé qu’une seule fois dans le AppModule
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {

    if (parentModule) {
      throw new Error('[SOW]: CoreModule est déjà chargé.');
    }
  }
  // le CoreModule ne peut être instancié qu’une fois, et depuis un module parent uniquement.
}
