import {HolderGeo} from './HolderGeo.model';

export class HolderAdress {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo?: HolderGeo;  // ? variable optionnel
  constructor(street: string, suite: string, city: string, zipcode: string, geo?: HolderGeo) {
    this.street = street;
    this.suite = suite;
    this.city = city;
    this.zipcode = zipcode;
    this.geo = geo;
  }
}
