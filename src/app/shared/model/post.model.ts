export class Post {
  id: number;
  constructor(public title: string,
              public content: string,
              public loveIts: number,
              public createdat: Date) {
  }
}
