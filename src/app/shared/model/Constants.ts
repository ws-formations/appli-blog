export class Constants {
  static readonly DD_MM_YYYY = 'dd/MM/yyyy';
  static readonly DATE_TIME_FMT = `${Constants.DD_MM_YYYY} hh:mm:ss`;
}
