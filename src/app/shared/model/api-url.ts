import {environment} from '../../../environments/environment';

export class APIURL {
  static baseUrl = environment.baseUrl;
}
