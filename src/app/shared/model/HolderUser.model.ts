import {HolderAdress} from './HolderAdress.model';
import {HolderCompany} from './HolderCompany.model';

export class HolderUser {
  id: number;
  name: string;
  username: string;
  email: string;
  address?: HolderAdress;  // ? variable optionnel
  phone: string;
  website: string;
  company?: HolderCompany;  // ? variable optionnel

  constructor(id: number, name: string, username: string, email: string, phone: string, website: string, address?: HolderAdress, company?: HolderCompany) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
    this.address = address;
    this.phone = phone;
    this.website = website;
    this.company = company;
  }
}
