export class HolderCompany {
  name: string;
  catchPhrase: string;
  bs: string;
  constructor(name: string, catchPhrase: string, bs: string) {
    this.name = name;
    this.catchPhrase = catchPhrase;
    this.bs = bs;
  }
}
