export class RestError {
  httpStatus: number;
  message: string;
  timestamp: string;
  path: string;
  errorParameters?: string[];  // ? variable optionnel
  constructor(httpStatus: number, message: string, timestamp: string, path: string, errorParameters?: string[]) {
    this.httpStatus = httpStatus;
    this.message = message;
    this.timestamp = timestamp;
    this.path = path;
    this.errorParameters = errorParameters;
  }
}
