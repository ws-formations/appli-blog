import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'toPositif'})
export class MinusPipe implements PipeTransform {
  transform(value: number): number {
    return value >= 0 ? value : Math.abs(value);
  }
}
