/*
 * Affiche la couleur correspondant au type du pokémon.
 * Prend en argument le type du pokémon.
 * Exemple d'utilisation:
 *   {{ pokemon.type | pokemonTypeColor }}
*/
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'blogTypeColor'})
export class BlogTypeColorPipe implements PipeTransform {

  transform(type: string): string {

    let color: string;

    switch (type) {
      case 'Like':
        color = 'text-dange';
        break;
      case 'Dislike':
        color = 'text-dange';
        break;
      default:
        color = 'grey';
        break;
    }

    return 'chip ' + color;

  }
}
