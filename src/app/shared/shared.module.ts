import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MinusPipe} from './pipe/MinusPipe';
import {RouterModule} from '@angular/router';
import {RequiredlabelDirective} from './directives/requiredlabel.directive';
import {HighlightDirective} from './directives/highlight.directive';
import {HeaderComponent} from './composants/bar/header/header.component';
import {ObsSubjectComponent} from './composants/note/obs-subject/obs-subject.component';
import {TranslateModule} from '@ngx-translate/core';
import {ObserverImgComponent} from './composants/note/observer-img/observer-img.component';
import {ObservablePipeoneComponent} from './composants/note/observable-pipeone/observable-pipeone.component';
import {PipeDateFormat} from './pipe/PipeDateFormat';
import {BlogTypeColorPipe} from './pipe/blog-type-color.pipe';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    MinusPipe,
    PipeDateFormat,
    BlogTypeColorPipe,
    RequiredlabelDirective,
    HighlightDirective,
    HeaderComponent,
    ObsSubjectComponent,
    ObserverImgComponent,
    ObservablePipeoneComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    NgbPaginationModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    MinusPipe,
    PipeDateFormat,
    BlogTypeColorPipe,
    RequiredlabelDirective,
    HighlightDirective,
    HeaderComponent,
    ObsSubjectComponent,
    ObserverImgComponent,
    AutocompleteLibModule,
    NgbPaginationModule,
    ObservablePipeoneComponent
  ]
})
export class SharedModule { }
