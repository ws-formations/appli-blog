import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-observer-img',
  templateUrl: './observer-img.component.html',
  styleUrls: ['./observer-img.component.scss']
})
export class ObserverImgComponent implements OnInit, OnDestroy {

  mesImages = ['ile.jpg'];
 //  mesImages = ['coeur.jpg', 'ile.jpg', 'soleil.jpg'];
  currentImage: string;
  monObservable: Observable<string>;
  imgSubscription: Subscription;
  constructor() { }

  ngOnInit(): void {
    this.observerImg();
  }

  /**
   * let monObservable = new Observable<string>();
   */
  createObservableImg(): Observable<string> {

    this.monObservable = new Observable(
      (observer) => {
        let i = 0;
        setInterval(
          () => {
            // On crée un flux continu d'images
            // à chaque fois dans mon observable on envoie un flux qui va contenir une image => this.mesImages[i]
            observer.next(this.mesImages[i]);
            if (i < this.mesImages.length - 1) {
              i++;
            }else {
              i = 0;
            }
            console.log('Index i = ' + i + ' Image Current = ' + this.mesImages[i]);
          }, 60000);

      }
    );
    return this.monObservable;
  }

  /**
   * On s'inscrit à l'observable
   */
  observerImg(): void {

    this.imgSubscription = this.createObservableImg().subscribe(
      (monImg) => {
        this.currentImage = monImg;
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!'); // pour ce exple cette ligne ne sera pas exécuter
      }
    );
  }
  /**
   * Pour éviter les bugs liés aux Observables comme ici un compteur à l'infini
   * Il est conseillé de stocker subscribe dans l'objet Subscription et appliquer unsubscribe()
   * ngOnDestroy():  se déclenche quand le component sera détruit
   */
  ngOnDestroy(): void {
    this.imgSubscription.unsubscribe();
  }
}
