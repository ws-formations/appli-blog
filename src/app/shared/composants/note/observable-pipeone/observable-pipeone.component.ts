import {Component, OnDestroy, OnInit} from '@angular/core';
import {from, Observable, Subscription} from 'rxjs';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-observable-pipeone',
  templateUrl: './observable-pipeone.component.html',
  styleUrls: ['./observable-pipeone.component.scss']
})
export class ObservablePipeoneComponent implements OnInit, OnDestroy {

  mesNombre = [1, 2, 3, 4, 5, 6, 7];
  currentNumber: number;
  monObservable: Observable<number>;
  nbSubscription: Subscription;

  srcObject = from([
    { fName: 'Sachin', lName: 'Tendulkar' },
    { fName: 'Rahul', lName: 'Dravid' },
    { fName: 'Saurav', lName: 'Ganguly' },
  ]);

  constructor() { }

  ngOnInit(): void {
    this.observerNumber();
    this.mapToSingleProperty();
  }

  mapToSingleProperty() {
    this.srcObject
      .pipe(
        map(data => {
        return data.fName + ' ' + data.lName;
      }))
      .subscribe(data => {
        console.log(data);
      });
  }

  /**
   * let monObservable = new Observable<string>();
   */
  createObservableNumber(): Observable<number> {

    this.monObservable = new Observable(
      (observer) => {
        // On crée un flux continu d'images
        // à chaque fois dans mon observable on envoie un flux qui va contenir une image => this.mesImages[i]
        for (const nb of this.mesNombre) {
          observer.next(nb);
        }
      }
    );
    return this.monObservable;
  }

  /**
   * On s'inscrit à l'observable
   * Res = 8, 10, 12, 14
   */
  observerNumber(): void {

    this.nbSubscription = this.createObservableNumber()
      .pipe(
           filter((data) => data > 3),
           map((val) => val as number * 2)
          )
      .subscribe(
      (monImg) => {
           this.currentNumber = monImg;
           console.log('currentNumber = ' + this.currentNumber);
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!'); // pour ce exple cette ligne ne sera pas exécuter
      }
    );
  }
  /**
   * Pour éviter les bugs liés aux Observables comme ici un compteur à l'infini
   * Il est conseillé de stocker subscribe dans l'objet Subscription et appliquer unsubscribe()
   * ngOnDestroy():  se déclenche quand le component sera détruit
   */
  ngOnDestroy(): void {
    this.nbSubscription.unsubscribe();
  }
}
