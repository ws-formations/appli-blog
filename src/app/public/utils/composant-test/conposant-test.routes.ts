import {Route} from '@angular/router';
import {ComposantTestComponent} from './composant-test.component';

export const composantTestRoutes: Route[] = [
  {
    path: '',
    component: ComposantTestComponent
  }
];
