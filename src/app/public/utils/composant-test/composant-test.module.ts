import {NgModule} from '@angular/core';
import {ComposantTestComponent} from './composant-test.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {composantTestRoutes} from './conposant-test.routes';

@NgModule({
  declarations: [ComposantTestComponent],
  imports: [
    RouterModule.forChild(composantTestRoutes),
    SharedModule,
    TranslateModule
  ],
  exports: [ComposantTestComponent]
})
export class ComposantTestModule { }

