import {Route} from '@angular/router';
import {PublicComponent} from './public.component';

export const PublicRoutes: Route[] = [
  {
    path: '', component: PublicComponent,
    children: [
      {path: '', loadChildren: () => import('../public/accueil/accueil.module').then(m => m.AccueilModule)},
      {path: 'accueil', loadChildren: () => import('../public/accueil/accueil.module').then(m => m.AccueilModule)},
      {path: 'auth', loadChildren: () => import('../public/auth/auth.module').then(m => m.AuthModule)},
      {path: 'memo', loadChildren: () => import('../public/utils/composant-test/composant-test.module').then(m => m.ComposantTestModule)}
    ]
  }
];
