import {NgModule} from '@angular/core';
import {AccueilComponent} from './accueil.component';
import {RouterModule} from '@angular/router';
import {AccueilRoutes} from './accueil.routes';
import {SharedModule} from '../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [AccueilComponent],
  imports: [
    RouterModule.forChild(AccueilRoutes),
    SharedModule,
    TranslateModule
  ],
  exports: [AccueilComponent]
})
export class AccueilModule { }
