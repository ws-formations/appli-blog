import {AccueilComponent} from './accueil.component';
import {Route} from '@angular/router';

export const AccueilRoutes: Route[] = [
  {
    path: '',
    component: AccueilComponent
  }
];
