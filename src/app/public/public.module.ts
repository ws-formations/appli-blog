import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {PublicComponent} from './public.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  imports: [RouterModule, FormsModule, SharedModule],
  declarations: [PublicComponent],
  exports: [PublicComponent]
})

export class PublicModule {
}
