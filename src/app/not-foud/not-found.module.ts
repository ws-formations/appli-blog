import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {NotFoudComponent} from './not-foud.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  imports: [SharedModule],
  declarations: [NotFoudComponent],
  exports: [NotFoudComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NotFoudModule {
}
