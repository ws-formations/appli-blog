import {Route} from '@angular/router';
import {NotFoudComponent} from './not-foud.component';

export const NotfoundRoutes: Route[] = [
  {path: '**', component: NotFoudComponent}
];
