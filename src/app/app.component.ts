import {Component} from '@angular/core';
import * as firebase from 'firebase';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public translate: TranslateService) {
    console.log('Environment Production ', environment.production); // Logs false for default environment
    console.log('Environment Actif ', environment.baseUrl); // Logs false for default environment
    translate.addLangs(['fr', 'en']);
    translate.setDefaultLang('fr');

    const firebaseConfig = {
    apiKey: 'AIzaSyC3IvoUd26ggH8iSSFlvtPzowk5N8uNIuk',
    authDomain: 'tutosangular.firebaseapp.com',
    databaseURL: 'https://tutosangular.firebaseio.com',
    projectId: 'tutosangular',
    storageBucket: 'tutosangular.appspot.com',
    messagingSenderId: '124311630723',
    appId: '1:124311630723:web:c25585c2ecb1ce23ef7cb6',
    measurementId: 'G-8M74B726TB'
  };

    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }

}
