import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PublicRoutes} from './public/public.routes';
import {NotfoundRoutes} from './not-foud/not-found.routes';
import {ProtectedRoutes} from './protected/protected.routes';

export const APP_ROUTING: Routes = [ ... PublicRoutes, ...ProtectedRoutes, ...NotfoundRoutes];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTING, { useHash: true })],
  exports: [RouterModule]
})
export class AppRouteModule {}
